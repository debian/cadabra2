Source: cadabra2
Section: math
Priority: optional
Maintainer: Gürkan Myczko <tar@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 dh-exec,
 dh-python,
 dvipng,
 libboost-all-dev,
 libgmp-dev,
 libgmp3-dev,
 libgtkmm-3.0-dev,
 libjsoncpp-dev,
 libpcre2-dev,
 libsqlite3-dev,
 python3-dev,
 python3-gmpy2,
 python3-matplotlib,
 python3-mpmath,
 python3-sympy,
 texlive,
 texlive-latex-extra,
 uuid-dev,
X-Python3-Version: >= 3.6
Rules-Requires-Root: no
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/myczko-guest/cadabra2.git
Vcs-Browser: https://salsa.debian.org/myczko-guest/cadabra2
Homepage: https://cadabra.science/

Package: cadabra2
Architecture: any
Depends:
 fonts-cmu,
 ${python3:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: field-theory motivated computer algebra system
 This is a computer algebra system designed specifically for the
 solution of problems encountered in field theory. It has extensive
 functionality for tensor polynomial simplification including
 multi-term symmetries, fermions and anti-commuting variables,
 Clifford algebras and Fierz transformations, implicit coordinate
 dependence, multiple index types and many more. The input format is
 a subset of TeX.
